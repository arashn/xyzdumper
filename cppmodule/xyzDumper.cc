/*
Highly Optimized Object-oriented Many-particle Dynamics -- Blue Edition
(HOOMD-blue) Open Source Software License Copyright 2009-2014 The Regents of
the University of Michigan All rights reserved.

HOOMD-blue may contain modifications ("Contributions") provided, and to which
copyright is held, by various Contributors who have granted The Regents of the
University of Michigan the right to modify and/or distribute such Contributions.

You may redistribute, use, and create derivate works of HOOMD-blue, in source
and binary forms, provided you abide by the following conditions:

* Redistributions of source code must retain the above copyright notice, this
list of conditions, and the following disclaimer both in the code and
prominently in any materials provided with the distribution.

* Redistributions in binary form must reproduce the above copyright notice, this
list of conditions, and the following disclaimer in the documentation and/or
other materials provided with the distribution.

* All publications and presentations based on HOOMD-blue, including any reports
or published results obtained, in whole or in part, with HOOMD-blue, will
acknowledge its use according to the terms posted at the time of submission on:
http://codeblue.umich.edu/hoomd-blue/citations.html

* Any electronic documents citing HOOMD-Blue will link to the HOOMD-Blue website:
http://codeblue.umich.edu/hoomd-blue/

* Apart from the above required attributions, neither the name of the copyright
holder nor the names of HOOMD-blue's contributors may be used to endorse or
promote products derived from this software without specific prior written
permission.

Disclaimer

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, AND/OR ANY
WARRANTIES THAT THIS SOFTWARE IS FREE OF INFRINGEMENT ARE DISCLAIMED.

IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

// we need to include boost.python in order to export ExampleUpdater to python
#include <boost/python.hpp>
using namespace boost::python;

// we need to include boost.bind for GPUWorker execution
#include <boost/bind.hpp>
using namespace boost;

#include <boost/smart_ptr/make_shared.hpp>

#include "xyzDumper.h"
using namespace std;

/*! \file xyzDumper.cc
    \brief Dump particle data as t x y z vx vy vz, each particle 6 columns
*/

// ********************************
// here follows the code for xyzDumper on the CPU

/*! \param sysdef System to dump particle data from
*/
xyzDumper::xyzDumper(boost::shared_ptr<SystemDefinition> sysdef, boost::shared_ptr<ParticleGroup> group, std::string filenamePrefix)
        : Updater(sysdef)
    {
    m_group = group;
    // Create output file streams
    setParams(filenamePrefix);
    // Create mapping from tag to local
    ArrayHandle<unsigned int> h_tag(m_pdata->getTags(), access_location::host, access_mode::read);
    mapTagToLocal.assign(m_pdata->getN(), 0);

    for (unsigned int i=0; i<m_group->getNumMembers(); i++)
        {   
        // Get the index of the current group member
        const unsigned int idx = m_group->getMemberIndex(i);
        // Get the tag of the current particle
        const unsigned int tag = h_tag.data[idx];
        // Save into mapping table
        mapTagToLocal[tag] = i;
        }
    }


/*! Perform the needed calculations to dump particle data
    \param timestep Current time step of the simulation
*/
void xyzDumper::update(unsigned int timestep)
    {
    if (m_prof) m_prof->push("xyzDumper");
    
    // Access the particle data for reading on the CPU
    assert(m_pdata);
    ArrayHandle<Scalar4> h_vel(m_pdata->getVelocities(), access_location::host, access_mode::read);
    ArrayHandle<Scalar4> h_pos(m_pdata->getPositions(), access_location::host, access_mode::read);
    ArrayHandle<int3> h_image(m_pdata->getImages(), access_location::host, access_mode::read);
    ArrayHandle<unsigned int> h_tag(m_pdata->getTags(), access_location::host, access_mode::read);

    // Acquire simulation box data
    const BoxDim& box = m_pdata->getBox();
    Scalar3 box_size = box.getL();

    // Loop over all particles in group and save particle data to file
    for (unsigned int i=0; i<m_group->getNumMembers(); i++)
        {
        // Get the index of the current group member
        const unsigned int idx = m_group->getMemberIndex(i);
        // Get the tag of the current particle
        const unsigned int tag = h_tag.data[idx];
        // Get the local index of the current particle
        (*m_xyzData[mapTagToLocal[tag]])<<timestep<<"\t"
        <<(h_pos.data[idx].x + h_image.data[idx].x*box_size.x)<<"\t"<<(h_pos.data[idx].y + h_image.data[idx].y*box_size.y)<<"\t"<<(h_pos.data[idx].z + h_image.data[idx].z*box_size.z)<<"\t"
        <<h_vel.data[idx].x<<"\t"<<h_vel.data[idx].y<<"\t"<<h_vel.data[idx].z<<std::endl;
        }
    
    if (m_prof) m_prof->pop();
    }


void xyzDumper::setParams(std::string filenamePrefix)
    {
        // Create a vector of output file streams for each particle in group
        m_xyzData.clear();
        m_xyzData.reserve(m_group->getNumMembers());
        for (unsigned int i=0; i<m_group->getNumMembers(); i++)
            {
            std::string filename = filenamePrefix + boost::lexical_cast<std::string>(i) + ".dat";
            m_xyzData.push_back(boost::make_shared<ofstream>(filename.c_str()));
            (*m_xyzData[m_xyzData.size()-1])<<std::scientific;
            (*m_xyzData[m_xyzData.size()-1]).precision(5);
            }
        
        std::cout<<"Saving xyz data for "<<m_group->getNumMembers()<<" particles to "<<filenamePrefix<<"<i>.dat"<<std::endl;
    }

void export_xyzDumper()
    {
    class_<xyzDumper, boost::shared_ptr<xyzDumper>, bases<Updater>, boost::noncopyable>
    ("xyzDumper", init< boost::shared_ptr<SystemDefinition>, boost::shared_ptr<ParticleGroup>, std::string >())
    ;
    }

